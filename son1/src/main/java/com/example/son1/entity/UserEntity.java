package com.example.son1.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * @Author Huang_ShengSen
 * @Date 2023/7/17 17:51
 * @PackageName:com.example.son1.entity
 * @ClassName: UserEntity
 * @Description: TODO
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@ToString
public class UserEntity {
    private String name;
    private int age;
    private String address;
}
