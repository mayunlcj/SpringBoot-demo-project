package com.example.son1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Son1Application {

    public static void main(String[] args) {
        SpringApplication.run(Son1Application.class, args);
    }

}
