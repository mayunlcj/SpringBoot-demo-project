package com.example.son2.service;

import com.example.son1.entity.UserEntity;
import org.springframework.stereotype.Service;

/**
 * @Author Huang_ShengSen
 * @Date 2023/7/17 17:56
 * @PackageName:com.example.son2.service
 * @ClassName: UserService
 * @Description: TODO
 * @Version 1.0
 */
@Service
public class UserService {
    public UserEntity getUser(){
        return new UserEntity("shengsen",23,"guangdo");
    }
}
