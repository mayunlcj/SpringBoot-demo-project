package com.example.web.controller;

import com.example.son2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Huang_ShengSen
 * @Date 2023/7/17 18:02
 * @PackageName:com.example.web.controller
 * @ClassName: UserController
 * @Description: TODO
 * @Version 1.0
 */
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "/test")
    public String test(){
        return userService.getUser().toString();
    }
}