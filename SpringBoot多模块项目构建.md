**SpringBoot多模块项目构建**

时间：2023/7/17

Springboot 多模块项目搭建 + 打包 jar 运行 + 打包 war 运行 + 一个启动类（有的项目是多模块多个启动类），为后续的 SpringCloud 项目作准备。

<img src="C:\Users\77939\AppData\Roaming\Typora\typora-user-images\image-20230717173138979.png" alt="image-20230717173138979" style="zoom: 80%;" />

![image-20230717173218578](C:\Users\77939\AppData\Roaming\Typora\typora-user-images\image-20230717173218578.png)

删除src文件，修改pom文件，添加<packaging>pom</packaging>

![image-20230717173403431](C:\Users\77939\AppData\Roaming\Typora\typora-user-images\image-20230717173403431.png)

创建 3 个子模块，分别为 son1、son2、web，不勾选任何依赖。

![image-20230717173707091](C:\Users\77939\AppData\Roaming\Typora\typora-user-images\image-20230717173707091.png)

创建好三个模块之后，在 father 项目父工程 pom.xml 添加依赖

```java
<modules>
    <module>son1</module>
    <module>son2</module>
    <module>web</module>
</modules>
```

![image-20230717173758418](C:\Users\77939\AppData\Roaming\Typora\typora-user-images\image-20230717173758418.png)

更换 pom.xml 插件，准备打包使用

```java
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.1</version>
            <configuration>
                <source>${java.version}</source>
                <target>${java.version}</target>
            </configuration>
        </plugin>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>2.19.1</version>
            <configuration>
                <skipTests>true</skipTests>    <!--默认关掉单元测试 -->
            </configuration>
        </plugin>
    </plugins>
</build>
```

修改子模块pom文件,这样就同样拥有了 lombok 以及 web 的依赖

```java
    <parent>
        <groupId>com.example</groupId>
        <artifactId>father</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </parent>
```

son1、son2删除

```java
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

```java
<properties> 
    <java.version>1.8</java.version> 
</properties>
```

添加：打包类型

```java
<packaging>jar</packaging>
```

在son1新建entity包，新建UserEntity.java文件，内容如下

```java
@Data
@AllArgsConstructor
@ToString
public class UserEntity {
    private String name;
    private int age;
    private String address;
}
```

son2 定义为 service 层，需要使用 son1 的实体类，然后依赖 son1，在 sun2 的 pom.xml 中配置

```java
<dependency>
    <groupId>com.example</groupId>
    <artifactId>son1</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

在son2中新建service包，新建Useservice.java文件，内容如下

```java
@Service
public class UserService {
    public UserEntity getUser(){
        return new UserEntity("shengsen",23,"guangdo");
    }
}
```

在web项目的pom文件中添加

```java
<dependency>
    <groupId>com.example</groupId>
    <artifactId>son1</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
<dependency>
    <groupId>com.example</groupId>
    <artifactId>son2</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

在web项目启动类上添加扫描，因为多个模块，web 启动类运行的话默认只运行扫描自己这个模块，导致找不到其他模块，所以添加一个扫描。

```java
@ComponentScan(basePackages = {"com.example.son2.service","com.example.web.controller"})
```

在web项目上新建controller包，新建UserController.java文件，内容如下

```java
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "/test")
    public String test(){
        return userService.getUser().toString();
    }
}
```

运行web项目，访问 http://localhost:8080/test 进行测试查看数据

![image-20230717180616572](C:\Users\77939\AppData\Roaming\Typora\typora-user-images\image-20230717180616572.png)

打包，在 web 项目 pom.xml 中插件更换为

```java
<build>
    <!--打包之后的名字-->
    <finalName>demo_project</finalName>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <mainClass>com.example.web.WebApplication</mainClass>
                <layout>ZIP</layout>
            </configuration>
            <executions>
                <execution>
                    <goals>
                        <goal>repackage</goal>
                    </goals>
                    <!-- <!–可以生成不含依赖包的不可执行Jar包–>
                     <configuration>
                         <classifier>exec</classifier>
                     </configuration>-->
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

![image-20230717181420116](C:\Users\77939\AppData\Roaming\Typora\typora-user-images\image-20230717181420116.png)

![image-20230717181451590](C:\Users\77939\AppData\Roaming\Typora\typora-user-images\image-20230717181451590.png)

去命令行启动项目，命令java -jar E:\project\IdeaProjects\father\web\target\demo_project.jar

![image-20230717181911264](C:\Users\77939\AppData\Roaming\Typora\typora-user-images\image-20230717181911264.png)

测试

![image-20230717181945863](C:\Users\77939\AppData\Roaming\Typora\typora-user-images\image-20230717181945863.png)